# Mini_Jeu_JAVA



### NOM DUCROCQ  
### Prenom Renaud  

## Description  
Ce projet a pour but la réalisation d'un mini jeu avec JAVA en utilisant l'interface graphique JAVAFX.  
C'est un projet scolaire réalisé en coordination avec le Professeur sous forme d'étapes afin de nous familiariser avec le langage orienté objet, et avec les outils graphiques JAVAFX. Le but final n'est donc pas d'avoir un jeu complet ni optimisé mais de prendre en main différents outils de développement.  

Merci à Gregory Bourguin Docteur en informatique, Maître de conférence LISIC, Enseignant à l'ULCO.  
Lien vers son site : [http://web.gregory-bourguin.fr/](http://web.gregory-bourguin.fr/)

Un petit apperçu du résultat:  
![image.png](image.png)  
![image-1.png](image-1.png)  
